<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Product Eav Select and Multiply Select Attributes Indexer resource model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
    extends Mage_Catalog_Model_Resource_Product_Indexer_Eav_Abstract
{
    const OLD_SUFFIX = '_old';
    const NEW_SUFFIX = '_new';

    /**
     * flag temporary is table created
     *
     * @var bool
     */
    private $_tmpTableCreated = false;

    /**
     * Initialize connection and define main index table
     *
     */
    protected function _construct()
    {
        $this->_init('catalog/product_index_eav', 'entity_id');
    }

    /**
     * Retrieve new index table name
     *
     * @return string
     */
    private function getNewTable()
    {
        return $this->getMainTable() . self::NEW_SUFFIX;
    }
    /**
     * Retrieve old index table name
     *
     * @return string
     */
    private function getOldTable()
    {
        return $this->getMainTable() . self::OLD_SUFFIX;
    }

    /**
     * exclude not visible items from select
     *
     * @param Varien_Db_Select $select
     * @param string $tableAlias
     * @return Varien_Db_Select $select
     */
    protected function excludeNonVisibleProducts(Varien_Db_Select $select, $tableAlias = 'i')
    {
        $condition = $this->_getIndexAdapter()->quoteInto('<> ?', Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
        $this->_addAttributeToSelect(
            $select,
            'visibility',
            $tableAlias . '.entity_id',
            $tableAlias . '.store_id',
            $condition
        );
        return $select;
    }

    /**
     * Clean up temporary index table
     *
     * magento runs per default delete from table which blows up
     * the mysql transaction log
     * this method isn't required anymore
     */
    public function clearTemporaryIndexTable()
    {
        return $this;
    }

    /**
     * Retrieve indexable eav attribute ids
     *
     * @param bool $multiSelect
     * @return array
     */
    protected function _getIndexableAttributes($multiSelect)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('ca' => $this->getTable('catalog/eav_attribute')), 'attribute_id')
            ->join(
                array('ea' => $this->getTable('eav/attribute')),
                'ca.attribute_id = ea.attribute_id',
                array())
            ->where($this->_getIndexableAttributesCondition());

        if ($multiSelect == true) {
            $select->where('ea.backend_type = ?', 'text')
                ->where('ea.frontend_input = ?', 'multiselect');
        } else {
            $select->where('ea.backend_type = ?', 'int')
                ->where('ea.frontend_input = ?', 'select');
        }

        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * Prepare data index for indexable attributes
     *
     * @param array $entityIds      the entity ids limitation
     * @param int $attributeId      the attribute id limitation
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
     */
    protected function _prepareIndex($entityIds = null, $attributeId = null)
    {
        $this->_prepareSelectIndex($entityIds, $attributeId);
        $this->_prepareMultiselectIndex($entityIds, $attributeId);

        return $this;
    }

    /**
     * Prepare data index for indexable select attributes
     *
     * @param array $entityIds      the entity ids limitation
     * @param int $attributeId      the attribute id limitation
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
     */
    /**
     * Prepare data index for indexable select attributes
     *
     * @param array $entityIds the entity ids limitation
     * @param int $attributeId the attribute id limitation
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
     */
    protected function _prepareSelectIndex($entityIds = null, $attributeId = null)
    {
        $adapter = $this->_getWriteAdapter();
        $idxTable = $this->getIdxTable();
        // prepare select attributes
        if (is_null($attributeId)) {
            $attrIds = $this->_getIndexableAttributes(false);
        } else {
            $attrIds = array($attributeId);
        }
        if (!$attrIds) {
            return $this;
        }
        /**@var $subSelect Varien_Db_Select */
        $subSelect = $adapter->select()
            ->from(
                array('s' => $this->getTable('core/store')),
                array('store_id', 'website_id')
            )
            ->joinLeft(
                array('d' => $this->getValueTable('catalog/product', 'int')),
                '1 = 1 AND d.store_id = 0',
                array('entity_id', 'attribute_id', 'value')
            )
            //added missing attribute filter
            ->where('s.store_id != 0 and d.attribute_id IN (?)', array_map('intval', $attrIds));
        if (!is_null($entityIds)) {
            $subSelect->where('d.entity_id IN(?)', array_map('intval', $entityIds));
        }
        /**@var $select Varien_Db_Select */
        $select = $adapter->select()
            ->from(
                array('pid' => new Zend_Db_Expr(sprintf('(%s)', $subSelect->assemble()))),
                array()
            )
            ->joinLeft(
                array('pis' => $this->getValueTable('catalog/product', 'int')),
                'pis.entity_id = pid.entity_id AND pis.attribute_id = pid.attribute_id AND pis.store_id = pid.store_id',
                array()
            )
            ->columns(
                array(
                    'pid.entity_id',
                    'pid.attribute_id',
                    'pid.store_id',
                    'value' => $adapter->getIfNullSql('pis.value', 'pid.value')
                )
            )
            ->where('pid.attribute_id IN(?)', $attrIds);
        $select->where(Mage::getResourceHelper('catalog')->getIsNullNotNullCondition('pis.value', 'pid.value'));
        /**
         * Add additional external limitation
         */
        Mage::dispatchEvent('prepare_catalog_product_index_select', array(
            'select' => $select,
            'entity_field' => new Zend_Db_Expr('pid.entity_id'),
            'website_field' => new Zend_Db_Expr('pid.website_id'),
            'store_field' => new Zend_Db_Expr('pid.store_id')
        ));
        $query = $select->insertFromSelect($idxTable);
        $adapter->query($query);
        return $this;
    }

    /**
     * Prepare data index for indexable multiply select attributes
     *
     * @param array $entityIds      the entity ids limitation
     * @param int $attributeId      the attribute id limitation
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
     */
    protected function _prepareMultiselectIndex($entityIds = null, $attributeId = null)
    {
        $adapter    = $this->_getWriteAdapter();

        // prepare multiselect attributes
        if (is_null($attributeId)) {
            $attrIds    = $this->_getIndexableAttributes(true);
        } else {
            $attrIds    = array($attributeId);
        }

        if (!$attrIds) {
            return $this;
        }

        // load attribute options
        $options = array();
        $select  = $adapter->select()
            ->from($this->getTable('eav/attribute_option'), array('attribute_id', 'option_id'))
            ->where('attribute_id IN(?)', $attrIds);
        $query = $select->query();
        while ($row = $query->fetch()) {
            $options[$row['attribute_id']][$row['option_id']] = true;
        }

        // prepare get multiselect values query
        $productValueExpression = $adapter->getCheckSql('pvs.value_id > 0', 'pvs.value', 'pvd.value');
        $select = $adapter->select()
            ->from(
                array('pvd' => $this->getValueTable('catalog/product', 'text')),
                array('entity_id', 'attribute_id'))
            ->join(
                array('cs' => $this->getTable('core/store')),
                '',
                array('store_id'))
            ->joinLeft(
                array('pvs' => $this->getValueTable('catalog/product', 'text')),
                'pvs.entity_id = pvd.entity_id AND pvs.attribute_id = pvd.attribute_id'
                    . ' AND pvs.store_id=cs.store_id',
                array('value' => $productValueExpression))
            ->where('pvd.store_id=?',
                $adapter->getIfNullSql('pvs.store_id', Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID))
            ->where('cs.store_id!=?', Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID)
            ->where('pvd.attribute_id IN(?)', $attrIds);

        $statusCond = $adapter->quoteInto('=?', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $this->_addAttributeToSelect($select, 'status', 'pvd.entity_id', 'cs.store_id', $statusCond);

        if (!is_null($entityIds)) {
            $select->where('pvd.entity_id IN(?)', $entityIds);
        }

        /**
         * Add additional external limitation
         */
        Mage::dispatchEvent('prepare_catalog_product_index_select', array(
            'select'        => $select,
            'entity_field'  => new Zend_Db_Expr('pvd.entity_id'),
            'website_field' => new Zend_Db_Expr('cs.website_id'),
            'store_field'   => new Zend_Db_Expr('cs.store_id')
        ));

        $i     = 0;
        $data  = array();
        $query = $select->query();
        while ($row = $query->fetch()) {
            $values = array_unique(explode(',', $row['value']));
            foreach ($values as $valueId) {
                if (isset($options[$row['attribute_id']][$valueId])) {
                    $data[] = array(
                        $row['entity_id'],
                        $row['attribute_id'],
                        $row['store_id'],
                        $valueId
                    );
                    $i ++;
                    if ($i % 10000 == 0) {
                        $this->_saveIndexData($data);
                        $data = array();
                    }
                }
            }
        }

        $this->_saveIndexData($data);
        unset($options);
        unset($data);

        return $this;
    }

    /**
     * Save a data to temporary source index table
     *
     * @param array $data
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Source
     */
    protected function _saveIndexData(array $data)
    {
        if (!$data) {
            return $this;
        }
        $adapter = $this->_getWriteAdapter();
        $adapter->insertArray($this->getIdxTable(), array('entity_id', 'attribute_id', 'store_id', 'value'), $data);
        return $this;
    }

    /**
     * create new table to prepare table rotation
     */
    protected function _prepareNewTable(){
        $table = $this->_getWriteAdapter()->createTableByDdl($this->getMainTable(), $this->getNewTable());
        //foreign keys should be unique so we need to change the names of the table
        $rpForeignKeys = new ReflectionProperty($table, '_foreignKeys');
        $rpForeignKeys->setAccessible(true);
        $fkTmp = array();
        foreach ($table->getForeignKeys() as $foreignKeyData) {
            $uuid = Mage::helper('index')->getUUID();
            $foreignKeyData['FK_NAME'] = $uuid;
            $fkTmp[$uuid] = $foreignKeyData;
        }
        $rpForeignKeys->setValue($table, $fkTmp);
        $this->_getWriteAdapter()->createTable($table);
    }

    /**
     * Retrieve temporary index table name
     *
     * @param string $table
     * @return string
     */
    public function getIdxTable($table = null)
    {
        //check we already have created a temp table
        if (false === $this->_tmpTableCreated) {
            $table = $this->_getWriteAdapter()->createTableByDdl(
                $this->getTable('catalog/product_eav_indexer_tmp'),
                $this->getTable('catalog/product_eav_indexer_tmp') . 'x'
            );
            $this->_getWriteAdapter()->createTemporaryTable($table);
            $this->_tmpTableCreated = true;
        }
        return $this->getTable('catalog/product_eav_indexer_tmp') . 'x';
    }

    public function syncData()
    {
        //clean up last rotation if there was an error
        $this->_getWriteAdapter()->dropTable($this->getNewTable());
        $this->_getWriteAdapter()->dropTable($this->getOldTable());
        try {
            $this->_prepareNewTable();
            //get columns mapping and insert data to new table
            $sourceColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getIdxTable()));
            $targetColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getNewTable()));
            $select = $this->_getIndexAdapter()->select()->from($this->getIdxTable(), $sourceColumns);
            //add join for bypassing disabled products and avoid heavy delete query
            $select = $this->excludeNonVisibleProducts($select, $this->getIdxTable());
            //disable index updates
            $this->_getWriteAdapter()->enableTableKeys($this->getNewTable());
            $this->insertFromSelect($select, $this->getNewTable(), $targetColumns, false);
            //sync parent child relation data @see _prepareRelationIndex
            $select = $this->_getWriteAdapter()->select()
                ->from(array('l' => $this->getTable('catalog/product_relation')), 'parent_id')
                ->join(array('cs' => $this->getTable('core/store')), '', array())
                ->join(array('i' => $this->getIdxTable()), 'l.child_id = i.entity_id AND cs.store_id = i.store_id', array('attribute_id', 'store_id', 'value'))
                ->group(array('l.parent_id', 'i.attribute_id', 'i.store_id', 'i.value'));
            /**
             * Add additional external limitation
             */
            Mage::dispatchEvent('prepare_catalog_product_index_select', array(
                'select' => $select,
                'entity_field' => new Zend_Db_Expr('l.parent_id'),
                'website_field' => new Zend_Db_Expr('cs.website_id'),
                'store_field' => new Zend_Db_Expr('cs.store_id')
            ));
            $select->order(new Zend_Db_Expr('NULL'));
            //add join for bypassing disabled products and avoid heavy delete query
            $select = $this->excludeNonVisibleProducts($select, 'i');
            $query = $this->_getWriteAdapter()->insertFromSelect($select, $this->getNewTable(), array(), Varien_Db_Adapter_Interface::INSERT_IGNORE);
            $this->_getWriteAdapter()->query($query);
            //enable index updates
            $this->_getWriteAdapter()->enableTableKeys($this->getNewTable());
            //rotate the tables
            $this->_getWriteAdapter()->renameTablesBatch(
                array(
                    array('oldName' => $this->getMainTable(), 'newName' => $this->getOldTable()),
                    array('oldName' => $this->getNewTable(), 'newName' => $this->getMainTable())
                )
            );
            //drop table to reclaim table space
            $this->_getIndexAdapter()->dropTable($this->getOldTable());
        } catch (Exception $e) {
            $this->_getWriteAdapter()->dropTable($this->getNewTable());
            $this->_getWriteAdapter()->dropTable($this->getOldTable());
            throw $e;
        }
        return $this;
    }

    /**
     * Rebuild index data by entities
     *
     *
     * @param int|array $processIds
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Abstract
     * @throws Exception
     */
    public function reindexEntities($processIds)
    {
        $adapter = $this->_getWriteAdapter();
        $this->clearTemporaryIndexTable();
        if (!is_array($processIds)) {
            $processIds = array($processIds);
        }
        $parentIds = $this->getRelationsByChild($processIds);
        if ($parentIds) {
            $processIds = array_unique(array_merge($processIds, $parentIds));
        }
        $childIds = $this->getRelationsByParent($processIds);
        if ($childIds) {
            $processIds = array_unique(array_merge($processIds, $childIds));
        }
        $this->_prepareIndex($processIds);
        $adapter->beginTransaction();
        try {
            $this->useDisableKeys(false);
            // remove old index
            $where = $adapter->quoteInto('entity_id IN(?)', array_map('intval', $processIds));
            $adapter->delete($this->getMainTable(), $where);
            //get columns mapping and insert data to new table
            $sourceColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getIdxTable()));
            $targetColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getMainTable()));
            $select = $this->_getIndexAdapter()->select()->from($this->getIdxTable(), $sourceColumns);
            //add join for bypassing disabled products and avoid heavy delete query
            $select = $this->excludeNonVisibleProducts($select, $this->getIdxTable());
            $query = $this->_getWriteAdapter()->insertFromSelect($select, $this->getMainTable(), $targetColumns, Varien_Db_Adapter_Interface::INSERT_IGNORE);
            $this->_getWriteAdapter()->query($query);
            //sync parent child relation data @see _prepareRelationIndex
            $select = $this->_getWriteAdapter()->select()
                ->from(array('l' => $this->getTable('catalog/product_relation')), 'parent_id')
                ->join(array('cs' => $this->getTable('core/store')), '', array())
                ->join(array('i' => $this->getIdxTable()), 'l.child_id = i.entity_id AND cs.store_id = i.store_id', array('attribute_id', 'store_id', 'value'))
                ->group(array('l.parent_id', 'i.attribute_id', 'i.store_id', 'i.value'));
            /**
             * Add additional external limitation
             */
            Mage::dispatchEvent('prepare_catalog_product_index_select', array(
                'select' => $select,
                'entity_field' => new Zend_Db_Expr('l.parent_id'),
                'website_field' => new Zend_Db_Expr('cs.website_id'),
                'store_field' => new Zend_Db_Expr('cs.store_id')
            ));
            $select->order(new Zend_Db_Expr('NULL'));
            //add join for bypassing disabled products and avoid heavy delete query
            $select = $this->excludeNonVisibleProducts($select, 'i');
            $query = $this->_getWriteAdapter()->insertFromSelect($select, $this->getMainTable(), array(), Varien_Db_Adapter_Interface::INSERT_IGNORE);
            $this->_getWriteAdapter()->query($query);
            $this->useDisableKeys(true);
            $adapter->commit();
        } catch (Exception $e) {
            $adapter->rollBack();
            throw $e;
        }
        return $this;
    }

    /**
     * Rebuild index data by attribute id
     * If attribute is not indexable remove data by attribute
     *
     *
     * @param int $attributeId
     * @param bool $isIndexable
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Abstract
     */
    public function reindexAttribute($attributeId, $isIndexable = true)
    {
        if (!$isIndexable) {
            $this->_removeAttributeIndexData($attributeId);
        } else {
            $this->_prepareIndex(null, $attributeId);
            $this->_getWriteAdapter()->beginTransaction();
            try {
                // remove index by attribute
                $where = $this->_getWriteAdapter()->quoteInto('attribute_id = ?', $attributeId);
                $this->_getWriteAdapter()->delete($this->getMainTable(), $where);
                //get columns mapping and insert data to new table
                $sourceColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getIdxTable()));
                $targetColumns = array_keys($this->_getWriteAdapter()->describeTable($this->getMainTable()));
                $select = $this->_getIndexAdapter()->select()->from($this->getIdxTable(), $sourceColumns);
                //add join for bypassing disabled products and avoid heavy delete query
                $select = $this->excludeNonVisibleProducts($select, $this->getIdxTable());
                $query = $this->_getWriteAdapter()->insertFromSelect($select, $this->getMainTable(), $targetColumns, Varien_Db_Adapter_Interface::INSERT_IGNORE);
                $this->_getWriteAdapter()->query($query);
                //sync parent child relation data @see _prepareRelationIndex
                $select = $this->_getWriteAdapter()->select()
                    ->from(array('l' => $this->getTable('catalog/product_relation')), 'parent_id')
                    ->join(array('cs' => $this->getTable('core/store')), '', array())
                    ->join(array('i' => $this->getIdxTable()), 'l.child_id = i.entity_id AND cs.store_id = i.store_id', array('attribute_id', 'store_id', 'value'))
                    ->group(array('l.parent_id', 'i.attribute_id', 'i.store_id', 'i.value'));
                /**
                 * Add additional external limitation
                 */
                Mage::dispatchEvent('prepare_catalog_product_index_select', array(
                    'select' => $select,
                    'entity_field' => new Zend_Db_Expr('l.parent_id'),
                    'website_field' => new Zend_Db_Expr('cs.website_id'),
                    'store_field' => new Zend_Db_Expr('cs.store_id')
                ));
                $select->order(new Zend_Db_Expr('NULL'));
                //add join for bypassing disabled products and avoid heavy delete query
                $select = $this->excludeNonVisibleProducts($select, 'i');
                $query = $this->_getWriteAdapter()->insertFromSelect($select, $this->getMainTable(), array(), Varien_Db_Adapter_Interface::INSERT_IGNORE);
                $this->_getWriteAdapter()->query($query);
                $this->_getWriteAdapter()->commit();
            } catch (Exception $e) {
                $this->_getWriteAdapter()->rollback();
                throw $e;
            }
        }
        return $this;
    }

    /**
     * Rebuild all index data
     *
     * we simply remove here _removeNotVisibleEntityFromIndex because we can handle this with a join
     *
     * @return Mage_Catalog_Model_Resource_Product_Indexer_Eav_Abstract
     */
    public function reindexAll()
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $this->_prepareIndex();
            $this->syncData();
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
        return $this;
    }
}
